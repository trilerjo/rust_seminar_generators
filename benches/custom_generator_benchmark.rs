#![feature(generators)]
use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use generators::nightly_gen::Gen;
use generators::our_gen::Generator;
use std::cell::RefCell;
use std::rc::Rc;

fn fibonacci_our(n: u64) -> u64 {
    let fib = Generator::new(Box::new(|g| {
        let mut i = 1;
        let mut j = 1;
        while i < n {
            g.suspend(i);
            i = i + j;
            j = i - j;
        }
    }));

    let mut result = 0;
    for i in fib {
        result += i;
    }
    result
}

fn fibonacci_nightly(n: u64) -> u64 {
    let fib = Gen::new(|| {
        Box::new(|| {
            let mut i = 1;
            let mut j = 1;
            while i < n {
                yield i;
                i = i + j;
                j = i - j;
            }
        })
    });
    let mut result = 0;
    for i in fib {
        result += i;
    }
    result
}

fn infinite_our(n: u64) -> u64 {
    let infinite = Generator::new(Box::new(|g| {
        let mut n = 1;
        loop {
            g.suspend(n); // yield a value in each iteration
            n = n + 1;
        }
    }));

    let mut result = Vec::new();
    for i in infinite {
        if i > n {
            break;
        }
        result.push(i);
    }
    result.iter().sum()
}

fn infinite_nightly(n: u64) -> u64 {
    let infinite_gen = Gen::new(|| {
        Box::new(|| {
            let mut n = 1;
            loop {
                yield n; // yield a value in each iteration
                n = n + 1;
            }
        })
    });

    let mut result = Vec::new();
    for i in infinite_gen {
        if i > n {
            break;
        }
        result.push(i);
    }
    result.iter().sum()
}

fn gen_inside_gen_our() {
    let vecs = Generator::new(Box::new(|g| {
        let fib = Generator::new(Box::new(|g| {
            let mut i = 1;
            let mut j = 1;
            while i < 1_000_000 {
                g.suspend(i);
                i = i + j;
                j = i - j;
            }
        }));

        for i in fib {
            let mut v = Vec::new();
            for j in i..i + 10 {
                v.push(j);
            }
            g.suspend(v);
        }
    }));
    let mut result = Vec::new();
    for i in vecs {
        result.push(i);
    }
}

fn gen_inside_gen_nightly() {
    let vecs_gen = Gen::new(|| {
        Box::new(move || {
            let fib_gen = Gen::new(|| {
                Box::new(move || {
                    let mut i = 1;
                    let mut j = 1;
                    while i < 1_000_000 {
                        yield i;
                        i = i + j;
                        j = i - j;
                    }
                })
            });

            for i in fib_gen {
                let mut v = Vec::new();
                for j in i..i + 10 {
                    v.push(j);
                }
                yield v;
            }
        })
    });

    let mut result = Vec::new();
    for i in vecs_gen {
        result.push(i);
    }
}

fn gen_rcs_our(n: u64) {
    let rcs = Generator::new(Box::new(|g| {
        let mut i: u64 = 0;
        while i < n {
            g.suspend(Rc::new(RefCell::new([i, i + 1, i + 2, i + 3])));
            i += 1;
        }
    }));

    let mut result = Vec::new();
    for i in rcs {
        result.push((*i).take());
    }
}

fn gen_rcs_nightly(n: u64) {
    let rcs_gen = Gen::new(|| {
        Box::new(move || {
            let mut i: u64 = 0;
            while i < n {
                yield (Rc::new(RefCell::new([i, i + 1, i + 2, i + 3])));
                i += 1;
            }
        })
    });

    let mut result = Vec::new();
    for rc in rcs_gen {
        result.push((*rc).take());
    }
}

fn gen_return_big_vec_nightly() {
    let rcs_gen = Gen::new(|| {
        Box::new(move || {
            let array: [i32; 100_000] = [0; 100_000];
            yield array;
        })
    });

    let mut result = Vec::new();
    for rc in rcs_gen {
        result.push(rc);
    }
}

fn gen_return_big_vec_our() {
    let rcs = Generator::new(Box::new(|g| {
        let array: [i32; 100_000] = [0; 100_000];
        g.suspend(array);
    }));

    let mut result = Vec::new();
    for i in rcs {
        result.push(i);
    }
}

fn gen_one_num_nightly() {
    let rcs_gen = Gen::new(|| {
        Box::new(move || {
            yield 5;
        })
    });

    let mut result = Vec::new();
    for rc in rcs_gen {
        result.push(rc);
    }
}

fn gen_one_num_our() {
    let rcs = Generator::new(Box::new(|g| {
        g.suspend(5);
    }));

    let mut result = Vec::new();
    for i in rcs {
        result.push(i);
    }
}

fn bench_fibs(c: &mut Criterion) {
    let mut group = c.benchmark_group("Fibo");
    for i in [20u64, 30u64, 50u64, 100u64, 2000u64, 100_000u64].iter() {
        group.bench_with_input(BenchmarkId::new("Our Generator", i), i, |b, i| {
            b.iter(|| fibonacci_our(*i))
        });
        group.bench_with_input(BenchmarkId::new("Nightly Generator", i), i, |b, i| {
            b.iter(|| fibonacci_nightly(*i))
        });
    }
    group.finish();
}

fn bench_infinit(c: &mut Criterion) {
    let mut group = c.benchmark_group("Infinite");
    for i in [200u64, 300u64, 500u64, 1_000u64, 2_000u64, 5_000u64, 10_000u64, 25_000u64, 50_000u64].iter() {
        group.bench_with_input(BenchmarkId::new("Our Generator", i), i, |b, i| {
            b.iter(|| infinite_our(*i))
        });
        group.bench_with_input(BenchmarkId::new("Nightly Generator", i), i, |b, i| {
            b.iter(|| infinite_nightly(*i))
        });
    }
    group.finish();
}

fn bench_gen_in_gen(c: &mut Criterion) {
    let mut group = c.benchmark_group("Gen_In_Gen");
    group.bench_function("OG - 1 Big array", move |b| {
        b.iter(|| gen_inside_gen_our());
    });
    group.bench_function("NG - 1 Big array", move |b| {
        b.iter(|| gen_inside_gen_nightly());
    });
    group.finish();
}

fn bench_rcs(c: &mut Criterion) {
    let mut group = c.benchmark_group("Gen RCS");
    for i in [200u64, 300u64, 500u64, 1_000u64, 2_000u64].iter() {
        group.bench_with_input(BenchmarkId::new("Our Generator", i), i, |b, i| {
            b.iter(|| gen_rcs_our(*i))
        });
        group.bench_with_input(BenchmarkId::new("Nightly Generator", i), i, |b, i| {
            b.iter(|| gen_rcs_nightly(*i))
        });
    }
    group.finish();
}

fn bench_big(c: &mut Criterion) {
    let mut group = c.benchmark_group("Big Array");
    group.bench_function("OG - 1 Big array", move |b| {
        b.iter(|| gen_return_big_vec_our());
    });
    group.bench_function("NG - 1 Big array", move |b| {
        b.iter(|| gen_return_big_vec_nightly());
    });
    group.finish();
}
fn bench_one(c: &mut Criterion){
    let mut group = c.benchmark_group("One Yield");
    group.bench_function("OG - One yield", move |b| {
        b.iter(|| gen_one_num_our());
    });
    group.bench_function("NG - One yield", move |b| {
        b.iter(|| gen_one_num_nightly());
    });
    group.finish();
}
criterion_group!(
    benches,
    //bench_fibs,
    bench_infinit,
    bench_gen_in_gen,
    //bench_rcs,
    bench_one,
    bench_big
);
criterion_main!(benches);

// fn main() {
//     fibonacci(10);
// }
