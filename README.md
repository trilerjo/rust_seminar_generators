# Generator

A Rust generator library that implements the iterator trait (often known by the `yield` keyword in other languages, such as [Nim](https://nim-lang.org/1.0.6/tut1.html#iterators)). Uses the [Boost.Context](https://www.boost.org/doc/libs/1_83_0/libs/context/doc/html/context/overview.html) C++ library via [context-rs](https://crates.io/crates/context) as a wrapper. An alternative implementation, based on the Rusts experimental `yield` snytax is provided

### Usage
 
```rust
use generators::our_gen::Generator;

fn main() {
    let fib = Generator::new(Box::new(|g| {
        let mut i = 1;
        let mut j = 1;
        while i < 200 {
            g.suspend(i); // yield i
            i = i + j;
            j = i - j;
        }
    }));

    for i in fib {
        println!("{}", i);
    }
}
```

Or using Rusts experimental generator support:

```rust
use generators::nightly_gen::Gen;

fn main() {
    let fib = Gen::new(|| {Box::new(|| {
        let mut i = 1;
        let mut j = 1;
        while i < 200 {
            yield i;
            i = i + j;
            j = i - j;
        }
    })});

    for i in fib {
        println!("{}", i);
    }
}
```
