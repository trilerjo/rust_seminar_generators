extern crate context;

use context::stack::ProtectedFixedSizeStack;
use context::{Context, Transfer};
use std::sync::atomic::compiler_fence;
use std::sync::atomic::Ordering;

pub struct Generator<'a, ReturnValue> {
    rv: Option<ReturnValue>,
    t: Option<Transfer>,
    is_finished: bool,
    f: Box<dyn Fn(&mut Generator<'a, ReturnValue>) + 'a>,
    #[allow(dead_code)]
    // the stack field is just there to keep it alive
    // while t still holds an unsafe reference to it
    stack: ProtectedFixedSizeStack,
}

extern "C" fn context_function<ReturnValue>(t: Transfer) -> ! {
    let generator_ptr = t.data as *mut Generator<ReturnValue>;

    unsafe {
        // Ensure the generator's transfer context is None before resuming
        assert!((*generator_ptr).t.is_none());
        // Set the generator's transfer context
        (*generator_ptr).t = Some(t);
        // Call the generator's function
        ((*generator_ptr).f)(&mut (*generator_ptr));
    }

    // Signal that the generator has finished
    unsafe {
        (*generator_ptr).is_finished = true;

        // Resume the generator's context and set the new transfer context
        (*generator_ptr).t = Some(
            (*generator_ptr)
                .t
                .take()
                .unwrap()
                .context
                .resume(generator_ptr as usize),
        );
    }

    // Ensure the generator has finished; this line will panic if not
    panic!("The generator should be finished by now...");
}

impl<'a, ReturnValue> Generator<'a, ReturnValue> {
    pub fn new(f: Box<dyn Fn(&mut Self) + 'a>) -> Self {
        let stack = ProtectedFixedSizeStack::default();
        let context_fn = context_function::<ReturnValue> as extern "C" fn(Transfer) -> !;
        let t = Some(Transfer::new(
            unsafe { Context::new(&stack, context_fn) },
            0,
        ));

        Self {
            rv: None,
            stack,
            t,
            is_finished: false,
            f,
        }
    }

    pub fn suspend(&mut self, rv: ReturnValue) {
        self.rv = Some(rv);
        compiler_fence(Ordering::AcqRel);

        if let Some(t) = self.t.take() {
            let data = t.data;
            self.t = Some(unsafe { t.context.resume(data) });
        }
    }
}

impl<'a, ReturnValue> Iterator for Generator<'a, ReturnValue> {
    type Item = ReturnValue;

    fn next(&mut self) -> Option<Self::Item> {
        // Check if the generator is finished.
        if self.is_finished {
            return None;
        }

        // Check if the transfer context is available.
        let t = match self.t.take() {
            Some(transfer) => transfer,
            None => return None,
        };

        // Resume the generator's context.
        self.t = Some(unsafe {
            t.context
                .resume(self as *mut Generator<ReturnValue> as usize)
        });

        // Yield the result.
        self.rv.take()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn gen_inside_gen() {
        let vecs = Generator::new(Box::new(|g| {
            let fib = Generator::new(Box::new(|g| {
                let mut i = 1;
                let mut j = 1;
                while i < 200 {
                    g.suspend(i);
                    i = i + j;
                    j = i - j;
                }
            }));

            for i in fib {
                let mut v = Vec::new();
                for j in i..i + 10 {
                    v.push(j);
                }
                g.suspend(v);
            }
        }));
        let mut result = Vec::new();
        for i in vecs {
            result.push(i);
        }
        assert!(
            result
                == vec![
                    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    [2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                    [3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                    [5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                    [8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
                    [13, 14, 15, 16, 17, 18, 19, 20, 21, 22],
                    [21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
                    [34, 35, 36, 37, 38, 39, 40, 41, 42, 43],
                    [55, 56, 57, 58, 59, 60, 61, 62, 63, 64],
                    [89, 90, 91, 92, 93, 94, 95, 96, 97, 98],
                    [144, 145, 146, 147, 148, 149, 150, 151, 152, 153],
                ]
        );
    }

    #[test]
    fn factorial() {
        let fact = Generator::new(Box::new(|g| {
            let mut n = 1;
            let mut f = 1;
            while n <= 10 {
                g.suspend(f);
                n = n + 1;
                f = f * n;
            }
        }));

        let mut result = Vec::new();
        for i in fact {
            result.push(i);
        }
        assert!(result == vec![1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800,]);
    }

    #[test]
    fn no_yield() {
        let empty = Generator::<i128>::new(Box::new(|_| {
            // do nothing
        }));

        let mut result = Vec::new();
        for i in empty {
            result.push(i);
        }
        assert!(result == vec![]);
    }

    #[test]
    fn only_one() {
        let single = Generator::new(Box::new(|g| {
            g.suspend(i32::MIN); // yield only one value
        }));

        let mut result = Vec::new();
        for i in single {
            result.push(i);
        }
        assert!(result == vec![i32::MIN]);
    }

    #[test]
    fn infinite() {
        let infinite = Generator::new(Box::new(|g| {
            let mut n = 1;
            loop {
                g.suspend(n); // yield a value in each iteration
                n = n + 1;
            }
        }));

        let mut result = Vec::new();
        for i in infinite {
            if i > 10 {
                break;
            }
            result.push(i);
        }
        assert!(result == vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    }

    #[test]
    fn gen_box() {
        let boxes = Generator::new(Box::new(|g| {
            g.suspend(Box::new("Hello"));
            g.suspend(Box::new("Bye"));
        }));

        let mut result = Vec::new();
        for i in boxes {
            result.push(*i);
        }
        assert!(result == vec!["Hello", "Bye"]);
    }

    use std::cell::RefCell;
    use std::rc::Rc;

    #[test]
    fn gen_rcs() {
        let rcs = Generator::new(Box::new(|g| {
            let mut i = 0;
            while i < 10 {
                g.suspend(Rc::new(RefCell::new([i, i + 1, i + 2, i + 3])));
                i += 1;
            }
            // g.suspend(Rc::new(RefCell::new([1, 2, 3]))); // yield an Rc<RefCell<i32>>
            // g.suspend(Rc::new(RefCell::new([4, 5, 6]))); // yield an Rc<RefCell<&str>>
            // g.suspend(Rc::new(RefCell::new([7, 8, 9]))); // yield an Rc<RefCell<[i32; 3]>>
        }));

        let mut result = Vec::new();
        for i in rcs {
            println!("{:?}", i); // this will print Rc<RefCell<T>> of different types
            result.push((*i).take());
        }
        //assert!(result == vec![[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
    }

    #[test]
    fn gen_empty_tuple() {
        let void_tuple = Generator::new(Box::new(|g| {
            g.suspend(());
            g.suspend(());
        }));

        let mut result = Vec::new();
        for i in void_tuple {
            result.push(i);
        }
        assert!(result == vec![(), ()]);
    }

    #[test]
    fn closure() {
        fn fibonacci(n: u64) -> u64 {
            let fib = Generator::new(Box::new(|g| {
                let mut i = 1;
                let mut j = 1;
                while i < n {
                    g.suspend(i);
                    i = i + j;
                    j = i - j;
                }
            }));
            let mut result = 0;
            for i in fib {
                result += i;
            }
            result
        }
        assert![fibonacci(10_000) == 17709];
    }
}
